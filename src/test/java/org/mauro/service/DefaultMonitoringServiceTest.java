package org.mauro.service;

import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mauro.client.AvailabilityGoloClient;
import org.mauro.model.MonitoringSetup;
import org.mauro.model.MonitoringSetupResponse;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mauro.model.MonitoringSetup.MIN_INTERVAL_MS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

//The unit test are oriented to test simple functionality,
//    not all test cases on this business class or negative test cases were tested.
//All classes with a business logic should be have unit tests.
final class DefaultMonitoringServiceTest {

	private final static String URL_TEST = "http://localhost:8081";

	private final AvailabilityGoloClient client = mock(AvailabilityGoloClient.class);
	private MonitoringService monitoringService;
	private MonitoringSetup monitoringSetup;

	@BeforeEach
	void setupEach() {
		//setting buisness class to test
		monitoringService = new MonitoringService(client);
	}

	@Nested
	@DisplayName("Given Golo APi server always available")
	class ServerAlwaysAvailable {

		@BeforeEach
		void setupXxx() {
			//monitoring with the minimum time and setting a valid URI string
			monitoringSetup = new MonitoringSetup(MIN_INTERVAL_MS + 1, URL_TEST);
			//mock client
			when(client.isAvailable(any(URL.class))).thenReturn(true);
		}

		@Test
		@SneakyThrows
		//note that this should be split in many subtest, but by time issue I put all together.
		void test() {
			//starting monitoring
			assertThat(monitoringService.startMonitoring(monitoringSetup))
				.isEqualTo(MonitoringSetupResponse.builder().monitoringSetup(monitoringSetup).isMonitorOn(true).build());

			//checking monitoring setup, monitor on expected
			assertThat(monitoringService.getMonitoringSetup())
				.isEqualTo(MonitoringSetupResponse.builder().monitoringSetup(monitoringSetup).isMonitorOn(true).build());

			//checking not unavailable periods.
			assertThat(monitoringService.getStatus().getPeriods()).isEmpty();

			//waiting at lease 2 polling updates
			Thread.sleep(MIN_INTERVAL_MS * 2 + 1);

			//stop monitoring.
			monitoringService.stopMonitoring();

			//checking monitoring setup, monitor off expected
			assertThat(monitoringService.getMonitoringSetup())
				.isEqualTo(MonitoringSetupResponse.builder().monitoringSetup(monitoringSetup).isMonitorOn(false).build());

			//checking not unavailable periods again.
			assertThat(monitoringService.getStatus().getPeriods()).isEmpty();
		}
	}

	@Nested
	@DisplayName("Given Golo APi server with not available all the time")
	class ServerNotAlwaysAvailable {

		@BeforeEach
		void setupXxx() {
			//monitoring with the minimum time and setting a valid URI string
			monitoringSetup = new MonitoringSetup(MIN_INTERVAL_MS + 1, URL_TEST);
			//mock client
			when(client.isAvailable(any(URL.class))).thenReturn(true, false, true, false);
		}

		@Test
		@SneakyThrows
		//note that this should be split in many subtest, but by time issue I put all together.
		void test() {
			//starting monitoring
			assertThat(monitoringService.startMonitoring(monitoringSetup))
				.isEqualTo(MonitoringSetupResponse.builder().monitoringSetup(monitoringSetup).isMonitorOn(true).build());

			//checking monitoring setup, monitor on expected
			assertThat(monitoringService.getMonitoringSetup())
				.isEqualTo(MonitoringSetupResponse.builder().monitoringSetup(monitoringSetup).isMonitorOn(true).build());

			//waiting at lease 2 polling updates
			Thread.sleep(MIN_INTERVAL_MS * 4 + 1);

			//stop monitoring.
			monitoringService.stopMonitoring();

			//checking monitoring setup, monitor off expected
			assertThat(monitoringService.getMonitoringSetup())
				.isEqualTo(MonitoringSetupResponse.builder().monitoringSetup(monitoringSetup).isMonitorOn(false).build());

			//checking not unavailable periods again.
			assertThat(monitoringService.getStatus().getPeriods()).hasSize(2);
		}
	}
}
