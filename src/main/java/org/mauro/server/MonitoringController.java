package org.mauro.server;

import org.mauro.model.MonitoringSetup;
import org.mauro.service.MonitoringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "monitoring")
//maybe add Swagger shoul be a nice idea!!!
public final class MonitoringController {

	private final MonitoringService monitoringService;

	@Autowired
	MonitoringController(final MonitoringService monitoringService) {
		this.monitoringService = monitoringService;
	}

	@RequestMapping(value = "/setup/start", method = POST, consumes = "application/json", produces = "application/json")
	ResponseEntity startMonitoring(@RequestBody() final MonitoringSetup monitoringSetup) {
		return new ResponseEntity<>(monitoringService.startMonitoring(monitoringSetup), OK);
	}

	@RequestMapping(value = "/setup/stop", method = POST)
	ResponseEntity stopMonitoring() {
		monitoringService.stopMonitoring();
		return new ResponseEntity<>(OK);
	}

	@RequestMapping(value = "/setup", method = GET, produces = "application/json")
	ResponseEntity getStatus() {
		return new ResponseEntity<>(monitoringService.getMonitoringSetup(), OK);
	}

	@RequestMapping(value = "/status", method = GET, produces = "application/json")
	ResponseEntity getMonitoringSetup() {
		return new ResponseEntity<>(monitoringService.getStatus(), OK);
	}
}
