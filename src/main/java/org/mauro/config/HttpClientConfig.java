package org.mauro.config;

import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@ToString
@Configuration
public class HttpClientConfig {
    private final int httpDefaultTimeout;
    private final int httpConnectionRequestTimeout;
    private final int httpValidateInactivityServerPing;
    private final boolean httpDisableCookieManagement;
    private final boolean httpDisableRedirectHandling;
    private final boolean httpDisableAutomaticRetries;
    private final boolean httpSystemPropertiesEnable;

    HttpClientConfig(@Value("${golo.http.client.httpDefaultTimeout:2000}") final Integer httpDefaultTimeout,
                     @Value("${golo.http.client.httpConnectionRequestTimeout:2000}") final Integer httpConnectionRequestTimeout,
                     @Value("${golo.http.client.httpValidateInactivityServerPing:1000}") final int httpValidateInactivityServerPing,
                     @Value("${golo.http.client.httpDisableCookieManagement:true}") final Boolean httpDisableCookieManagement,
                     @Value("${golo.http.client.httpDisableRedirectHandling:false}") final Boolean httpDisableRedirectHandling,
                     @Value("${golo.http.client.httpDisableAutomaticRetries:false}") final Boolean httpDisableAutomaticRetries,
                     @Value("${golo.http.client.httpSystemPropertiesEnable:true}") final Boolean httpSystemPropertiesEnable) {
        this.httpDefaultTimeout = httpDefaultTimeout;
        this.httpConnectionRequestTimeout = httpConnectionRequestTimeout;
        this.httpValidateInactivityServerPing = httpValidateInactivityServerPing;
        this.httpDisableCookieManagement = httpDisableCookieManagement;
        this.httpDisableRedirectHandling = httpDisableRedirectHandling;
        this.httpDisableAutomaticRetries = httpDisableAutomaticRetries;
        this.httpSystemPropertiesEnable = httpSystemPropertiesEnable;
    }
}
