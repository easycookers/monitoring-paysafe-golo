package org.mauro.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.mauro.config.HttpClientConfig;
import org.mauro.model.GoloAvailableStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URL;

import static org.springframework.http.HttpStatus.OK;

/**
 * This could be several layers, if we want abstract some business... As http layer and client issues.
 */
@Service
public class AvailabilityGoloClient {

	private final static Logger logger = LoggerFactory.getLogger(AvailabilityGoloClient.class);

	private final RestTemplate restTemplateClient;

    @Autowired
	private AvailabilityGoloClient(final HttpClientConfig config, final ObjectMapper mapper) {
		logger.debug("Configuring a new availability Golo client");

		//create restTemplate client.
        restTemplateClient = new RestTemplate(new HttpComponentsClientHttpRequestFactory(getHttpClient(config)));

        //Assign object mapper converter.
        restTemplateClient.getMessageConverters()
                .stream()
                .filter(converter -> converter instanceof MappingJackson2HttpMessageConverter)
                .forEach(converter -> ((MappingJackson2HttpMessageConverter) converter).setObjectMapper(mapper));
		logger.debug("Availability Golo client created!!!");
	}

    private CloseableHttpClient getHttpClient(final HttpClientConfig config) {
        // Setting request config
        final RequestConfig requestConfig = RequestConfig
                .custom()
                .setConnectTimeout(config.getHttpDefaultTimeout())
                .setConnectionRequestTimeout(config.getHttpConnectionRequestTimeout())
                .build();

        // Building httpclient
        final HttpClientBuilder httpClientsBuilder = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(requestConfig);
        if (config.isHttpDisableCookieManagement()) {
            httpClientsBuilder.disableCookieManagement();
        }
        if (config.isHttpDisableRedirectHandling()) {
            httpClientsBuilder.disableRedirectHandling();
        }
        if (config.isHttpDisableAutomaticRetries()) {
            httpClientsBuilder.disableAutomaticRetries();
        }
        if (config.isHttpSystemPropertiesEnable()) {
            httpClientsBuilder.useSystemProperties();
        }
        return httpClientsBuilder.build();
    }


	/**
	 * Check the availability of GOLO API.
	 * @param url Golo Api url
	 * @return True if it's available or false if not. In exception cases it returns false.
	 */
	public boolean isAvailable(final URL url) {
    	try {
		    logger.debug("sending request to check availability");

		    //send request
			final ResponseEntity<GoloAvailableStatus> goloAvailable =
				restTemplateClient.getForEntity(url.toURI(), GoloAvailableStatus.class);

			//check response
			if(goloAvailable.getStatusCode() != OK || !goloAvailable.hasBody()) {
				return false;
			}
			final boolean availability = goloAvailable.getBody().isAvailable();
		    logger.debug("new availability checked, new value: {}", availability);
			return availability;
		} catch(Exception ex) {
    		//in exception cases, return unavailable ('false').
		    logger.warn("An exception during request to check availability. Exception: {}", ex.getMessage());
		    return false;
		}
    }
}
