package org.mauro.repository;

import org.mauro.model.Status;
import org.mauro.model.UnavailablePeriod;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.ZoneOffset.UTC;

/**
 * This could be a cache object...
 */
public final class StatusRepository {

	private final List<UnavailablePeriod> periods = new ArrayList<>();
	private final ZonedDateTime lastStartMonitoring = ZonedDateTime.now(UTC);
	private volatile UnavailablePeriod cachePeriod;

	private StatusRepository() { }

	public static StatusRepository builder() {
		return new StatusRepository();
	}

	/**
	 * Update the current status if required.
	 * @param status new status to assign if change
	 */
	public void updateStatus(final boolean status) {
		if(cachePeriod == null && !status) {
			cachePeriod = UnavailablePeriod.builder(ZonedDateTime.now(UTC));
			periods.add(cachePeriod);
		} else if(cachePeriod != null && status) {
			cachePeriod.setEndTime(ZonedDateTime.now(UTC));
			cachePeriod = null;
		}
	}

	/**
	 * Get status info.
	 * @return all unavailable status and the last startMonitoring time
	 */
	public Status getStatus() {
		return Status
			.builder()
			.lastStartedTime(lastStartMonitoring)
			.periods(periods)
			.build();
	}
}
