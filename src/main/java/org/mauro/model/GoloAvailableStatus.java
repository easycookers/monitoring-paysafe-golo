package org.mauro.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import lombok.Value;

@Value
@ToString
//Immutable answer bean from golo API request.
public final class GoloAvailableStatus {

	//String is replaced by a boolean just to simplify the logic.
	private final boolean available;

	@JsonCreator
	public GoloAvailableStatus(@JsonProperty("status") final String status) {
		this.available = status.equals("READY");
	}
}
