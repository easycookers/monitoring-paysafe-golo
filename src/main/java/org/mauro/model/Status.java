package org.mauro.model;

import lombok.Builder;
import lombok.Singular;
import lombok.ToString;
import lombok.Value;

import java.time.ZonedDateTime;
import java.util.List;

@Value
@Builder
@ToString
//Immutable Status object
public final class Status {
	//last datetime UTC when the monitoring was started
	private final ZonedDateTime lastStartedTime;
	//list of all periods when the service was unavailable
	@Singular
	private final List<UnavailablePeriod> periods;
}
