package org.mauro.model;

import lombok.ToString;
import lombok.Value;

@Value
@ToString
//Immutable monitoring setup bean, used to receive the configuration.
public final class MonitoringSetup {
	public static final int MIN_INTERVAL_MS = 50;

	//interval in milis
	private final int intervalMs;
	//url to post request
	private final String url;
}
