package org.mauro.model;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;

@Builder
@Value
@ToString
//Immutable monitoring setup bean response, used to give a feedback in requests.
public final class MonitoringSetupResponse {
	//Configuration received.
	private final MonitoringSetup monitoringSetup;
	//True if monitoring scheduler is on
	private final boolean isMonitorOn;
}
