package org.mauro.model;

import lombok.Getter;
import lombok.ToString;
import org.mauro.model.exception.MonitoringException;

import java.time.ZonedDateTime;

@Getter
@ToString
//Immutable period, note that this allows setting each UTC datetime just one time.
public final class UnavailablePeriod {
	//When the service started to be unavailable. this CANNOT be null.
	private final ZonedDateTime startTime;
	//When the service was available. Note that this value could be null if the service is currently unavailable.
	private ZonedDateTime endTime;

	private UnavailablePeriod(final ZonedDateTime startTime) {
		this.startTime = startTime;
	}

	public static UnavailablePeriod builder(final ZonedDateTime startTime) {
		return new UnavailablePeriod(startTime);
	}

	/**
	 * Set the end. can be called just one time.
	 * @param endTime end time of unavailable service.
	 * @Throw MonitoringException if something does not match with the expected functionality
	 */
	public void setEndTime(final ZonedDateTime endTime) {
		if(endTime == null) {
			throw new MonitoringException("EndDate Cannot be null");
		}
		if(this.endTime != null) {
			throw new MonitoringException("EndDate was already set in this period!!!");
		}
		this.endTime = endTime;
	}
}
