package org.mauro.model.exception;

import lombok.Getter;

import static org.mauro.model.exception.ErrorType.MONITORING_ERROR;

@Getter
public final class MonitoringException extends IllegalStateException {
	private final ErrorType errorType = MONITORING_ERROR;

	public MonitoringException(final String message) {
		super(message);
	}
}
