package org.mauro.model.exception;

import lombok.Getter;

@Getter
public enum ErrorType {
	BAD_ARGUMENTS("01"),
	MONITORING_NEVER_CONFIGURED("02"),
	MONITORING_ERROR("03"),
	UNKNOWN_ERROR("04");

	final String errorNumber;

	ErrorType(final String errorNumber) {
		this.errorNumber = errorNumber;
	}
}
