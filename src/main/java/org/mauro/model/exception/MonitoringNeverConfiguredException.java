package org.mauro.model.exception;

import lombok.Getter;

import static org.mauro.model.exception.ErrorType.MONITORING_NEVER_CONFIGURED;

@Getter
public final class MonitoringNeverConfiguredException extends RuntimeException {
	private final ErrorType errorType = MONITORING_NEVER_CONFIGURED;

	public MonitoringNeverConfiguredException() {
		super("Monitor not configured, please start to continue");
	}
}
