package org.mauro.service;

import org.mauro.client.AvailabilityGoloClient;
import org.mauro.model.MonitoringSetup;
import org.mauro.model.MonitoringSetupResponse;
import org.mauro.model.Status;
import org.mauro.model.exception.MonitoringBadParametersException;
import org.mauro.model.exception.MonitoringNeverConfiguredException;
import org.mauro.repository.StatusRepository;
import org.mauro.tools.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.Closeable;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Service
public final class MonitoringService implements Closeable {

	private final static Logger logger = LoggerFactory.getLogger(MonitoringService.class);

	private final AvailabilityGoloClient client;

	private MonitoringSetup monitoringSetup;
	private StatusRepository repository;
	private ScheduledExecutorService executor;

	@Autowired
	MonitoringService(final AvailabilityGoloClient client) {
		this.client = client;
	}

	@PreDestroy
	/**
	 * this is used to stop the executor thread running in background.
	 */
	public void cleanUp() {
		executor.shutdownNow();
	}

	/**
	 * This start the monitoring
	 * @param monitoringSetup monitoring setup, timing scheduler & Api url
	 * @return monitoring setup
	 */
	public MonitoringSetupResponse startMonitoring(final MonitoringSetup monitoringSetup) {
		//validate arguments
		Validator.throwBadArgumentIfInvalidMonitorSetup(monitoringSetup);
		this.monitoringSetup = monitoringSetup;

		//logging
		logger.info("Start monitoring service. period(ms): {}, uri: {}",
		            monitoringSetup.getIntervalMs(),
		            monitoringSetup.getUrl());

		//Assign Url
		final URL url;
		try {
			url = new URL(monitoringSetup.getUrl());
		} catch(Exception ex) {
			logger.error("Error assigning request uri, exception: {}", ex.getMessage());
			throw new MonitoringBadParametersException("Error assigning request uri", ex);
		}

		//Stop if it's running before reconfiguring
		stopMonitoring();

		//starting monitoring
		repository = StatusRepository.builder();
		repository.updateStatus(client.isAvailable(url));
		//just 1 thread at time
		executor = Executors.newScheduledThreadPool(1);
		executor.scheduleAtFixedRate(() -> repository.updateStatus(client.isAvailable(url)),
									 monitoringSetup.getIntervalMs(),
									 monitoringSetup.getIntervalMs(),
									 MILLISECONDS);
		return buildMonitoringSetupResponse();
	}

	/**
	 * This stop the monitoring
	 */
	public void stopMonitoring() {
		if(isMonitorOn()) {
			executor.shutdown();
		}
	}

	/**
	 * this is to return the status.
	 * @return status
	 */
	public Status getStatus() {
		if(repository == null) {
			throw new MonitoringNeverConfiguredException();
		}
		return repository.getStatus();
	}

	/**
	 * this return the current config
	 * @return monitoring config
	 */
	public MonitoringSetupResponse getMonitoringSetup() {
		if(monitoringSetup == null) {
			throw new MonitoringNeverConfiguredException();
		}
		return buildMonitoringSetupResponse();
	}

	private MonitoringSetupResponse buildMonitoringSetupResponse() {
		return MonitoringSetupResponse
			.builder()
			.monitoringSetup(monitoringSetup)
			.isMonitorOn(isMonitorOn())
			.build();
	}

	private boolean isMonitorOn() {
		return executor != null && !executor.isShutdown();
	}

	@Override
	/**
	 * closeable class, nice to have when we have externals calls, externals access or I/O access...
	 */
	public void close() throws IOException {
		cleanUp();
	}
}
