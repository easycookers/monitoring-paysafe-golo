package org.mauro.tools;

import org.apache.commons.lang3.StringUtils;
import org.mauro.model.MonitoringSetup;
import org.mauro.model.exception.MonitoringBadParametersException;

import static org.mauro.model.MonitoringSetup.MIN_INTERVAL_MS;

public final class Validator {

	/**
	 * throw a Monitoring bad argument exception if:
	 * 		'monitoringSetup' is null or
	 * 		'monitoringSetup.url' is blank or
	 * 		'monitoringSetup.getIntervalMs' is smaller than minimum value (50 ms)
	 * @param monitoringSetup value to check
	 */
	public static void throwBadArgumentIfInvalidMonitorSetup(final MonitoringSetup monitoringSetup) {
		throwBadArgumentIfNull(monitoringSetup, "Body");
		throwBadArgumentIfBlank(monitoringSetup.getUrl(), "monitoringSetup.Url");
		if(monitoringSetup.getIntervalMs() < MIN_INTERVAL_MS) {
			throw new MonitoringBadParametersException("monitoringSetup.intervalMs Cannot be less than %n.",
													   MIN_INTERVAL_MS);
		}
	}

	/**
	 * throw a Monitoring bad argument exception if value is null
	 * @param value value to check
	 * @param valueName name of the variable to check, just to logging proposing
	 */
	public static void throwBadArgumentIfNull(final Object value, final String valueName) {
		if(value == null) {
			throw new MonitoringBadParametersException("%s cannot be null.", valueName);
		}
	}

	/**
	 * throw a Monitoring bad argument exception if value is blank
	 * @param value value to check
	 * @param valueName name of the variable to check, just to logging proposing
	 */
	public static void throwBadArgumentIfBlank(final String value, final String valueName) {
		if(StringUtils.isBlank(value)) {
			throw new MonitoringBadParametersException("%s cannot be blank.", valueName);
		}
	}
}
