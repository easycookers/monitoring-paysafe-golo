# Golo availability checker
It is based on spring boot microservice.

## Requirements
* java 8
* Maven 3

## COMPILING & RUN UNIT-TEST

```
mvn clean package
```

## USAGE

### Run jar

```
java -jar target/monitoring-paysafe-golo-1.0.0.jar
```

## API

* POST http://localhost:8081/monitoring/setup/start
Start the monitoring services
```
Body
{
	"interval_ms": 200,
	"url": "https://api.test.paysafe.com/accountmanagement/monitor"
}

```
Note that the minimum value of `interval_ms` is 50.
```
Response example
{
    "monitoring_setup": {
        "interval_ms": 200,
        "url": "https://api.test.paysafe.com/accountmanagement/monitor"
    },
    "monitor_on": true
}
```

* POST http://localhost:8081/monitoring/setup/stop
Stop the monitoring

* GET http://localhost:8081/monitoring/setup
return the monitoring config setup
```
Response example
{
    "monitoring_setup": {
        "interval_ms": 200,
        "url": "https://api.test.paysafe.com/accountmanagement/monitor"
    },
    "monitor_on": true
}
```

* GET http://localhost:8081/monitoring/status
return the monitoring status
```
Response example
{
    "lastStartedTime": "2018-08-02T05:38:21.591Z",
    "periods":[
        {
            "startTime": "2018-08-02T05:38:21.653Z",
            "endTime": "2018-08-02T05:38:21.703Z"
        },
        {
            "startTime": "2018-08-02T05:38:21.754Z",
        }
    ]
}
```